package com.nau.kursova;

import java.util.Arrays;

public class JacobiAlgorithm extends Algorithm {
    private static final int MAX_ITERATIONS = 1000;
    private final double[][] matrix;
    private final StringBuilder resultString = new StringBuilder();

    public JacobiAlgorithm(double[][] elements, double[] values) {
        matrix = new double[elements.length][elements[0].length + 1];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if(j == elements[0].length) {
                    matrix[i][j] = values[i];
                } else {
                    matrix[i][j] = elements[i][j];
                }
            }
        }
    }

    private String solve() {
        int iterations = 0;
        int n = matrix.length;
        double epsilon = 1e-15;
        double[] X = new double[n]; // Approximations
        double[] P = new double[n]; // Prev
        Arrays.fill(X, 0);
        while (true) {
            try {
                // needs for show difference between different algorithms
                // without sleeping it takes less than 1 ms to calculate all data
                Thread.sleep(10);
            } catch (InterruptedException ignore) { }
            for (int i = 0; i < n; i++) {
                double sum = matrix[i][n]; // b_n

                for (int j = 0; j < n; j++)
                    if (j != i)
                        sum -= matrix[i][j] * P[j];

                // Update x_i but it's no used in the next row calculation
                // but up to de next iteration of the method
                X[i] = 1/ matrix[i][i] * sum;
            }
            System.out.print("X" + iterations + " = {");
            for (int i = 0; i < n; i++)
                System.out.print(X[i] + " ");
            System.out.println("}");
            iterations++;
            if (iterations == 1)
                continue;
            boolean stop = true;
            for (int i = 0; i < n && stop; i++)
                if (Math.abs(X[i] - P[i]) > epsilon)
                    stop = false;
            if (stop || iterations == MAX_ITERATIONS) {
                for (int i = 0; i < n; i++) {
                    resultString
                            .append("x")
                            .append(i + 1)
                            .append(" = ")
                            .append(X[i])
                            .append("\n");
                }
                resultString
                        .append(String.format("За %d ітерацій.", iterations - 1));
                break;
            }
            P = X.clone();
        }
        return resultString.toString();
    }

    public String getName() {
        return "Метод простих ітерацій";
    }

    @Override
    public String findSolution() {
        return solve();
    }
}
