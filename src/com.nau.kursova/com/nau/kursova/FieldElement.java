package com.nau.kursova;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class FieldElement extends HBox {
    private Label label;
    private TextField field;

    public FieldElement(String defaultValue, String label) {
        super(5);
        this.label = new Label(label);
        this.label.setFont(Font.font("Arial", FontWeight.NORMAL, 14));

        this.field = new TextField();
        this.field.setText(defaultValue);
        this.field.setMaxWidth(40);
        this.field.setFont(Font.font("Arial", FontWeight.NORMAL, 14));

        this.getChildren().addAll(this.field, this.label);
        this.setAlignment(Pos.CENTER);
    }

    public Label getLabel() {
        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }

    public TextField getField() {
        return field;
    }

    public void setField(TextField field) {
        this.field = field;
    }
}
