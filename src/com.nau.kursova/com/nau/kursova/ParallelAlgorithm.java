package com.nau.kursova;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ParallelAlgorithm extends Algorithm {
    private final double[][] elements;
    private final double[] values;
    private final double[] answers;
    private final StringBuilder resultString = new StringBuilder();

    public ParallelAlgorithm(double[][] elements, double[] values) {
        this.elements = elements;
        this.values = values;
        this.answers = new double[values.length];
    }

    public String getName() {
        return "Паралельний алгоритм";
    }

    @Override
    public String findSolution() {
        int size = elements.length;

        ExecutorService es = Executors.newFixedThreadPool(size);
        Future[] futures = new Future[size];

        int cores = Math.max(Runtime.getRuntime().availableProcessors(), 1);

        for (int row = 0; row < size; row++) {
            double value = elements[row][row];
            for (int col = row + 1; col < size; col++) {
                elements[row][col] /= value;
            }

            values[row] /= value;
            elements[row][row] = 1.0;
            int step = (size - row + cores - 1) / cores;
            for (int innerRow = row + 1; innerRow < size; innerRow += step) {
                int row_ = row;
                int begin = innerRow, end = Math.min(size, innerRow + step);
                futures[innerRow] = es.submit(() -> {
                    try {
                        // needs for show difference between different algorithms
                        // without sleeping it takes less than 1 ms to calculate all data
                        Thread.sleep(10);
                    } catch (InterruptedException ignore) { }
                    for (int innerRow_ = begin; innerRow_ < end; innerRow_ ++) {
                        double innerValue = elements[innerRow_][row_];
                        for (int innerCol = row_ + 1; innerCol < size; innerCol++) {
                            elements[innerRow_][innerCol] -= innerValue * elements[row_][innerCol];
                        }
                        values[innerRow_] -= elements[innerRow_][row_] * values[row_];
                        elements[innerRow_][row_] = 0.0;
                    }
                });
            }
            for (int innerRow = row + 1; innerRow < size; innerRow += step) {
                try {
                    futures[innerRow].get();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
            }
        }

        for (int back = size - 1; back >= 0; back--) {
            answers[back] = values[back];
            for (int i = back - 1; i >= 0; i--) {
                values[i] -= answers[back] * elements[i][back];
            }
        }
        es.shutdown();

        for (int i = 0; i < answers.length; i++) {
            resultString
                    .append("x")
                    .append(i + 1)
                    .append(" = ")
                    .append(answers[i])
                    .append("\n");
        }

        resultString.setLength(resultString.length() - 1);

        return resultString.toString();
    }
}
