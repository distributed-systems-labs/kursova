package com.nau.kursova;

public class UsualAlgorithm extends Algorithm {
    private final double[][] elements;
    private final double[] values;
    private final StringBuilder resultString = new StringBuilder();

    public UsualAlgorithm(double[][] elements, double[] values) {
        this.elements = elements;
        this.values = values;
    }

    public String getName() {
        return "Звичайний алгоритм";
    }

    @Override
    public String findSolution() {
        int N = elements.length;
        for (int p = 0; p < N; p++) {

            try {
                // needs for show difference between different algorithms
                // without sleeping it takes less than 1 ms to calculate all data
                Thread.sleep(10);
            } catch (InterruptedException ignore) { }

            int max = p;

            for (int i = p + 1; i < N; i++) {
                if (Math.abs(elements[i][p]) > Math.abs(elements[max][p])) {
                    max = i;
                }
            }

            double[] temp = elements[p]; elements[p] = elements[max]; elements[max] = temp;
            double t = values[p]; values[p] = values[max]; values[max] = t;

            if (Math.abs(elements[p][p]) <= 1e-10) {
                resultString.append("Немає рішень");
                return resultString.toString();
            }

            for (int i = p + 1; i < N; i++) {
                double alpha = elements[i][p] / elements[p][p];
                values[i] -= alpha * values[p];
                for (int j = p; j < N; j++) {
                    elements[i][j] -= alpha * elements[p][j];
                }
            }
        }

        double[] x = new double[N];
        for (int i = N - 1; i >= 0; i--) {
            double sum = 0.0;
            for (int j = i + 1; j < N; j++) {
                sum += elements[i][j] * x[j];
            }
            x[i] = (values[i] - sum) / elements[i][i];
        }

        if (N < elements[0].length) {
            resultString.append("Нескінченна кількість рішень.");
            return resultString.toString();
        } else {
            for (int i = 0; i < N; i++) {
                resultString
                        .append("x")
                        .append(i + 1)
                        .append(" = ")
                        .append(x[i])
                        .append("\n");
            }
        }
        resultString.setLength(resultString.length() - 1);

        return resultString.toString();
    }
}
