package com.nau.kursova;

public abstract class Algorithm {
    public String name;
    public double[][] elements;
    public double[] values;

    public abstract String findSolution();
    public abstract String getName();

}
