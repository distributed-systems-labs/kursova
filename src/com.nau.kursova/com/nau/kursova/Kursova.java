package com.nau.kursova;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Kursova extends Application {
    private final int N = 4;
    private final int M = 4;
    private final Display display = new Display();
    private final Button smbtParallel = new Button("Паралельний розрахунок");
    private final Button smbtUsual = new Button("Звичайний розрахунок");
    private final Button smbtSeidel = new Button("Метод Зейделя");
    private final Button smbtIteration = new Button("Метод простих ітерацій");
    private final double[][] input = new double[N][M];
    private final double[] values = new double[M];
    private final FieldElement[][] fields = new FieldElement[4][5];
    private final String[][] defaultValues = new String[][]{
        new String[]{"10", "-5", "1", "5", "21"},
        new String[]{"2", "30", "-21", "5", "6"},
        new String[]{"2", "-8", "23", "12", "5"},
        new String[]{"2", "-8", "-2", "-14", "6"},
    };

    public static void main(String[] args) {
        launch(args);
    }

    private Text buildTitle() {
        Text text = new Text("Варіант №3. Метод Гаусса");
        text.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        text.setFill(Color.TOMATO);
        text.setLayoutY(30);
        text.setLayoutX(240);
        return text;
    }

    private GridPane buildExpression() {
        GridPane pane = new GridPane();
        pane.setHgap(6);
        pane.setVgap(10);
        pane.setLayoutX(30);
        pane.setLayoutY(50);


        for (int i = 0; i < defaultValues.length; i++) {
            for (int j = 0; j < defaultValues[i].length; j++) {
                boolean isLast = j == defaultValues[i].length - 1;
                String sign = j < defaultValues[i].length - 2 ? "+" : "=";
                FieldElement field = new FieldElement(defaultValues[i][j], isLast ? "," : "x" + (j + 1));
                int gridRow = j * 2;
                fields[i][j] = field;
                pane.add(field, gridRow, i);
                if (!isLast) {
                    pane.add(new Text(sign), gridRow + 1, i);
                }
            }
        }

        return pane;
    }

    private Pane buildRootPane() {
        Pane rootPane = new Pane();
        display.setLayoutX(420);
        smbtParallel.setLayoutX(20);
        smbtParallel.setLayoutY(200);
        smbtUsual.setLayoutX(20);
        smbtUsual.setLayoutY(240);
        smbtSeidel.setLayoutX(20);
        smbtSeidel.setLayoutY(280);
        smbtIteration.setLayoutX(20);
        smbtIteration.setLayoutY(320);


        rootPane.getChildren()
            .addAll(
                buildTitle(),
                buildExpression(),
                smbtParallel,
                smbtUsual,
                smbtSeidel,
                smbtIteration,
                display
            );
        return rootPane;
    }

    private void parseInput() {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                input[i][j] = Double.parseDouble(fields[i][j].getField().getText());
            }
        }
        for (int i = 0; i < N; i++) {
            values[i] = Double.parseDouble(fields[i][M].getField().getText());
        }
    }

    private void runAlgorithm(Algorithm algorithm) {
        display.print(String.format(">>>> Вибран %s", algorithm.getName()));
        long startTime = System.currentTimeMillis();
        display.print(algorithm.findSolution());
        long endTime = System.currentTimeMillis();
        display.print("Час виконання: " + (endTime - startTime) + "ms");
        display.print("");
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Курсова робота. Кузнєцов Олексій");
        primaryStage.setResizable(false);

        Pane rootPane = buildRootPane();

        Scene scene = new Scene(rootPane, 700, 360, Color.TRANSPARENT);
        primaryStage.setScene(scene);
        primaryStage.show();

        smbtUsual.setOnAction((ActionEvent event) -> {
            parseInput();
            UsualAlgorithm algorithm = new UsualAlgorithm(input, values);
            runAlgorithm(algorithm);
        });

        smbtParallel.setOnAction((ActionEvent event) -> {
            parseInput();
            ParallelAlgorithm algorithm = new ParallelAlgorithm(input, values);
            runAlgorithm(algorithm);
        });

        smbtSeidel.setOnAction((ActionEvent event) -> {
            parseInput();
            SeidelAlgorithm algorithm = new SeidelAlgorithm(input, values);
            runAlgorithm(algorithm);
        });

        smbtIteration.setOnAction((ActionEvent event) -> {
            parseInput();
            JacobiAlgorithm algorithm = new JacobiAlgorithm(input, values);
            runAlgorithm(algorithm);
        });
    }

}
