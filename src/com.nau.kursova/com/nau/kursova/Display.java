package com.nau.kursova;

import javafx.scene.control.TextArea;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;

public class Display extends TextArea {
    public Display() {
        super();
        this.setPrefSize(250, 280);
        this.setFont(Font.font("Arial", FontPosture.ITALIC, 12));
        this.setLayoutY(50);
        this.setWrapText(true);
    }

    public void print(String message) {
        this.appendText("\n" + message);
        setScrollTop(Double.MAX_VALUE);
    }
}
